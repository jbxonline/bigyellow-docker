SQL Anywhere 17.0 - Versionshinweise für Unix und Mac OS X

Copyright (c) 2015 SAP SE oder ein SAP-Konzernunternehmen. Alle Rechte vorbehalten.


SQL Anywhere 17 installieren
----------------------------

1. Wechseln Sie zu dem erstellten Verzeichnis und starten Sie das
   Installationsskript, indem Sie die folgenden Befehle eingeben:
        cd ga1700
        ./setup

   Eine vollständige Liste der verfügbaren Installationsoptionen erhalten 
   Sie, indem Sie den folgenden Befehl eingeben:
        ./setup -h

2. Befolgen Sie die Anweisungen des Installationsprogramms.


Installationshinweise 
---------------------

o Derzeit keine Einträge.


Dokumentation
-------------

Die Dokumentation finden Sie auf DocCommentXchange unter der folgenden Adresse:
    http://dcx.sap.com

DocCommentXchange ist eine Online-Community für den Zugang zur
SQL Anywhere-Dokumentation im Internet mit Diskussionsforum. DocCommentXchange ist das
Standarddokumentationsformat für SQL Anywhere 17.


o The CREATE ODATA PRODUCER statement incorrectly states that the
  SERVICE ROOT clause is optional. This clause is required.


o The documentation about updating the DBD::SQLAnywhere Perl Driver
  is incorrect. For information about updating the driver, refer to
  %SQLANY17%\SDK\Perl\readme.txt.


o  UltraLite New Feature - A new parameter named 'kdf_iterations' is now available when you are
   using database encryption. The value is the number of iterations (in thousands)
   of the "key derivation function". This value is the database encryption
   passphrase (the DBKEY parameter) that is processed on database startup. The key
   derivation function makes it more difficult to access an encrypted database by
   prolonging each attack attempt. The default iteration count values are
   30 (thousand) on Mac OS X and 5 (thousand) elsewhere.

   There are two cases when an explicit setting is required:

   1. You are using a very slow device and UltraLite takes too
      long to start with encryption. For example: kdf_iterations=1

   2. You are using a high-end computer and want added security. For
      example, when running on a Windows or Linux desktop: kdf_iterations=100

   This parameter is specified only at database creation.


SQL Anywhere-Forum
------------------

Das SQL Anywhere-Forum ist eine Website zum Austausch von Fragen und Antworten
über die SQL Anywhere-Software sowie zum Kommentieren von und Abstimmen über
die Fragen und Antworten anderer Benutzer. Besuchen Sie das SQL Anywhere-Forum unter:
    http://sqlanywhere-forum.sap.com.


Umgebungsvariablen für SQL Anywhere 17 festlegen
------------------------------------------------

Jeder Benutzer der Software muss die erforderlichen SQL Anywhere-
Umgebungsvariablen festlegen. Diese hängen vom jeweiligen Betriebssystem ab und werden
in der Dokumentation unter "SQL Anywhere-Server - Datenbankadministration >
Konfiguration Ihrer Datenbank > SQL Anywhere-Umgebungsvariablen" beschrieben.


Versionshinweise für SQL Anywhere 17
------------------------------------


SQL Anywhere-Server
-------------------

o Derzeit keine Einträge.


Administrationstools
--------------------

o When installing SQL Anywhere on 64-bit Linux machines, the default option
  is to install the 64-bit version of the graphical administration tools
  (SQL Central, Interactive SQL, and the MobiLink Profiler).

  Sie können auch 32-Bit-Administrationstools installieren. Dies Option gilt nur für
  OEMs, die die 32-Bit-Dateien für den Weitervertrieb benötigen.

  Running the 32-bit administration tools on 64-bit Linux is not supported.

o Um Java Access Bridge für die Administrationstools zu aktivieren,
  bearbeiten Sie die Datei "accessibility.properties"
  und entkommentieren Sie die letzten zwei Zeilen.

  Die Datei wird wie folgt angezeigt:
  #
  # Load the Java Access Bridge class into the JVM
  #
  #assistive_technologies=com.sun.java.accessibility.AccessBridge
  #screen_magnifier_present=true

o To use the adminsitration tools on Mac OS X distributions, install
  Java 1.7. It is available from:

    http://www.oracle.com/technetwork/java/index.html

o The graphical administration tools Interactive SQL, SQL Central, and
  the MobiLink Profiler) can crash on startup if they are run on a Solaris
  SPARC 10 computer. This behavior occurs if the user is logged in with a
  Simplified Chinese, UTF-8 language setting (zh_CN.UTF-8) and is related
  o the input method editor (IME) that is used for that language.

  There are two workarounds:

  Option 1:
    Melden Sie sich mit einer anderen Spracheinstellung an, z.B. "zh_CN.GB18030".

  Option 2:
    If you have to use zh_CN.UTF-8, then terminate the IME processes before
    running the graphical administration tools by running the following
    command from a Terminal window:
        pkill iiim*


o If characters do not display correctly in the Interactive SQL utility when
  you input data from a UTF8BIN database on SuSE Linux systems, you need to
  install a Unicode font.

   1. Go to "All Settings".
   2. In the "System" category, click "YaST" (its icon
      is an aardvark). Provide the root password to start YaST.
   3. In YaST, click "Software Management" (its icon is a box that is half
      white and half green with a Novell red letter "N" on it).
   4. In the "YaST 2" window, type "unicode font", and then
      click "Search".
   5. In the "Package" list (top right corner of the window),
      check everything ("efont-unicode-bitmap-fonts", "arphic-ukai-fonts",
      and so on), and then click the "Accept" button in the bottom right
      corner of the window.
   6. Reboot and retry the operation.


MobiLink
--------

o The MobiLink server requires an ODBC driver to communicate with the
  consolidated databases. The recommended ODBC drivers for a supported
  consolidated database can be found at:
    http://scn.sap.com/docs/DOC-63337

o Informationen zu den von MobiLink unterstützten Plattformen finden Sie unter:
    http://scn.sap.com/docs/DOC-35654

o Wenn Sie den MobiLink-Server mit Java 1.7 unter Mac OS X ausführen möchten,
  setzen Sie die Option -jrepath auf den vollständigen Pfad zur Datei libjvm.dylib. Beispiel:

  -sljava\(-jrepath `/usr/libexec/java_home -v 1.7`/jre/lib/server/libjvm.dylib\)


Relay Server
------------

o Using the Relay Server with Apache 2.4 is not recommended as it
  triggers the behavior identified in Apache Bugzilla issue 53555 (see
  https://bz.apache.org/bugzilla/show_bug.cgi?id=53555).
  Apache 2.2 is the recommended version.


UltraLite
---------

o Derzeit keine Einträge.


Betriebssystemunterstützung
---------------------------

o Unterstützung von Direct I/O und THP durch RedHat Enterprise Linux 6 -
  RedHat Linux 6 weist einen möglichen Bug in der THP-Funktion (Transparent
  Huge Pages) auf, die in dieser Version des Betriebssystems eingeführt wurde,
  bei Verwendung mit Direct I/O. Das wahrscheinlichste Auftreten dieses Bugs in
  SQL Anywhere ist Assertierung 200505 (Prüfsummenfehler auf Seite X). 
Red Hat-Bug 891857 wurde erstellt, um dieses Problem zu verfolgen.

  To work around this issue, SQL Anywhere avoids using Direct I/O on this
  operating system. To use Direct I/O, disable THP by running following command:
       echo never > /sys/kernel/mm/redhat_transparent_hugepage/enabled

o 64-bit Linux support - Some 64-bit Linux operating systems do not include
  preinstalled 32-bit compatability libraries. To use 32-bit software,
  you may need to install 32-bit compatability libraries for your Linux
  distribution. For example, on Ubuntu, run the following command:
	sudo apt-get install ia32-libs

Unter RedHat führen Sie aus:
	yum install glibc.i686
	yum install libXrenderer.so.1
	yum install libpk-gtk-module.so
	yum install libcanberra-gtk2.i686
	yum install gtk2-engines.i686

o Linux-Unterstützung für dbsvc - Das Dienstprogramm dbsvc
erfordert LSB init-Funktionen.
  Bei einigen Linux-Betriebssystemen sind diese Funktionen
nicht standardmäßig vorinstalliert.
  Wenn Sie dbsvc verwenden wollen, müssen diese Funktionen für Ihre
Linux-Distribution installiert werden.  Führen Sie beispielsweise unter Fedora
  den folgenden Befehl aus:
	yum install redhat-lsb redhat-lsb.i686

o SELinux support - If you are having problems running SQL Anywhere on SELinux,
  then you have the following options:

o Benennen Sie die gemeinsam genutzten Bibliotheken um, damit sie geladen werden können. 
Dies funktioniert unter Red Hat Enterprise Linux 5, hat aber
    den Nachteil, dass die SELinux-Funktionen nicht verwendet werden.
	find $SQLANY17 -name "*.so" | xargs chcon -t textrel_shlib_t 2>/dev/null

o Installieren Sie die mit SQL Anywhere 17 bereitgestellte Richtlinie. Im
 selinux-Verzeichnis der Installation befinden sich Quelldateien der
    Richtlinie. Anweisungen zum Erstellen und Installieren dieser Richtlinie
    finden Sie in der README-Datei in diesem Verzeichnis.

  o Write your own policy. You can use the policy provided with
    SQL Anywhere 17 as a starting point.

  o Deaktivieren Sie SELinux:
        /usr/sbin/setenforce 0

o Threads und Semaphore - Die Art der in der Software verwendeten Threads und
  Semaphore kann ziemlich wichtig sein, da es bei einigen Systemen zu
  Knappheit dieser Ressourcen kommen kann.

    o Unter Linux, AIX, HP-UX und Mac OS X verwendet SQL Anywhere pthreads
      (POSIX-Threads) und System V-Semaphore.

      Note: On platforms where System V semaphores are used, if the database
      server or a client application is terminated with SIGKILL, then System V
      semaphores are leaked. Clean them up by using the ipcrm command.
      In addition, client applications that terminate using the _exit() system
      call also leak System V semaphores unless the SQL Anywhere client
      libraries (such as ODBC and DBLib) are unloaded before this call.

o Verarbeitung von Alarmsignalen - Dies ist nur von Interesse, wenn Sie
  Non-Threaded-Anwendungen entwickeln und SIGALRM- oder SIGIO-Handler verwenden.

  SQL Anywhere uses a SIGALRM and a SIGIO handler in non-threaded
  clients and starts up a repeating alarm (every 200 ms). For correct behavior,
  SQL Anywhere must be allowed to handle these signals.

  If you define a SIGALRM or SIGIO handler before loading any SQL Anywhere
  libraries, then SQL Anywhere chains to these handlers.
  If you define a handler after loading any SQL Anywhere libraries,
  then chain from the SQL Anywhere handlers.

  If you use the TCP/IP communication protocol, SQL Anywhere uses
  SIGIO handlers in only non-threaded clients. This handler is always
  installed, but it is used only if your application makes use of TCP/IP.

o Unter Red Hat Enterprise Linux werden einige Zeichen des privaten
  Nutzungsbereichs in SQL Central, Interactive SQL (dbisql), dem MobiLink-
  Profiler oder dem SQL Anywhere-Monitor möglicherweise nicht richtig angezeigt.

  Für die Unicode-Codepoints "U+E844" und "U+E863" (als Zeichen für private
  Nutzung reserviert) werden in keiner der mit der Red Hat-Linux-Distribution
  ausgelieferten Truetype-Schriften Glyphen bereitgestellt. Die betreffenden
  Zeichen sind vereinfachte chinesische Schriftzeichen und in der
  Linux-Distribution Red Flag (chinesische Distribution) im Rahmen der Schrift
  zysong.ttf (DongWen-Song) verfügbar.

