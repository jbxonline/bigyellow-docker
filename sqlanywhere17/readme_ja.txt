SQL Anywhere 17.0 UNIX 版および Mac OS X 版リリースノート

Copyright (c) 2015 SAP SE or an SAP affiliate company. All rights reserved.


SQL Anywhere 17 のインストール
--------------------------

1. 作成されたディレクトリに移動し、次のコマンドを実行して
   設定スクリプトを開始します。
        cd ga1700
        ./setup

   使用可能な設定オプションのリストを表示するには、
   次のコマンドを実行します。
./setup -h

2. セットアッププログラムの指示に従います。


インストールに関する注意事項
------------------

o 現時点では何もありません。


マニュアル
-------------

マニュアルは DocCommentXchange にあります。アドレスは次のとおりです。
    http://dcx.sap.com

DocCommentXchange は Web 上の SQL Anywhere マニュアルを参照して
議論するためのオンラインコミュニティです。DocCommentXchange は 
SQL Anywhere 17 のデフォルトのマニュアルフォーマットです。


o The CREATE ODATA PRODUCER statement incorrectly states that the
  SERVICE ROOT clause is optional. This clause is required.


o The documentation about updating the DBD::SQLAnywhere Perl Driver
  is incorrect. For information about updating the driver, refer to
  %SQLANY17%\SDK\Perl\readme.txt.


o  UltraLite New Feature - A new parameter named 'kdf_iterations' is now available when you are
   using database encryption. The value is the number of iterations (in thousands)
   of the "key derivation function". This value is the database encryption
   passphrase (the DBKEY parameter) that is processed on database startup. The key
   derivation function makes it more difficult to access an encrypted database by
   prolonging each attack attempt. The default iteration count values are
   30 (thousand) on Mac OS X and 5 (thousand) elsewhere.

   There are two cases when an explicit setting is required:

   1. You are using a very slow device and UltraLite takes too
      long to start with encryption. For example: kdf_iterations=1

   2. You are using a high-end computer and want added security. For
      example, when running on a Windows or Linux desktop: kdf_iterations=100

   This parameter is specified only at database creation.


SQL Anywhere フォーラム
------------------

SQL Anywhere フォーラムは、SQL Anywhere ソフトウェアに関する質問や
回答を投稿できる Web サイトです。他の投稿者の質問やその回答にコメントや
評価を加えることもできます。SQL Anywhere フォーラムの URL は次のとおりです。
    http://sqlanywhere-forum.sap.com


SQL Anywhere 17 の環境変数の設定
-------------------------------------------------

ソフトウェアを使用するユーザごとに、SQL Anywhere の環境変数を設定する
必要があります。必要な環境変数はオペレーティングシステムによって異なり、
『SQL Anywhere サーバ - データベース管理』>
「データベースの設定」>「SQL Anywhere の環境変数」で説明しています。


SQL Anywhere 17 のリリースノート
---------------------------------


SQL Anywhere サーバ
-------------------

o 現時点では何もありません。


管理ツール
--------------------

o When installing SQL Anywhere on 64-bit Linux machines, the default option
  is to install the 64-bit version of the graphical administration tools
  (SQL Central, Interactive SQL, and the MobiLink Profiler).

  32 ビットの管理ツールをインストールするというオプションもあります。
 ただし、このオプションは、32 ビットファイルの再ディストリビューションを必要とする OEM の場合に限ります。

  Running the 32-bit administration tools on 64-bit Linux is not supported.

o 管理ツール用の Java Access Bridge を有効にするには、
  accessibility.properties ファイルを編集して最後の 2 行をコメント解除します。

  ファイルには次のように表示されます。
  #
  # Load the Java Access Bridge class into the JVM
  #
  #assistive_technologies=com.sun.java.accessibility.AccessBridge
  #screen_magnifier_present=true

o To use the adminsitration tools on Mac OS X distributions, install
  Java 1.7. It is available from:

    http://www.oracle.com/technetwork/java/index.html

o The graphical administration tools Interactive SQL, SQL Central, and
  the MobiLink Profiler) can crash on startup if they are run on a Solaris
  SPARC 10 computer. This behavior occurs if the user is logged in with a
  Simplified Chinese, UTF-8 language setting (zh_CN.UTF-8) and is related
  o the input method editor (IME) that is used for that language.

  There are two workarounds:

  オプション 1:
"zh_CN.GB18030" などの別の言語設定を使用してログインします。

  Option 2:
    If you have to use zh_CN.UTF-8, then terminate the IME processes before
    running the graphical administration tools by running the following
    command from a Terminal window:
        pkill iiim*


o If characters do not display correctly in the Interactive SQL utility when
  you input data from a UTF8BIN database on SuSE Linux systems, you need to
  install a Unicode font.

   1. Go to "All Settings".
   2. In the "System" category, click "YaST" (its icon
      is an aardvark). Provide the root password to start YaST.
   3. In YaST, click "Software Management" (its icon is a box that is half
      white and half green with a Novell red letter "N" on it).
   4. In the "YaST 2" window, type "unicode font", and then
      click "Search".
   5. In the "Package" list (top right corner of the window),
      check everything ("efont-unicode-bitmap-fonts", "arphic-ukai-fonts",
      and so on), and then click the "Accept" button in the bottom right
      corner of the window.
   6. Reboot and retry the operation.


Mobile Link
--------

o The MobiLink server requires an ODBC driver to communicate with the
  consolidated databases. The recommended ODBC drivers for a supported
  consolidated database can be found at:
    http://scn.sap.com/docs/DOC-63337

o Mobile Link でサポートされているプラットフォームの詳細については、
    http://scn.sap.com/docs/DOC-35654
    を参照してください。

o Mac OS X で Java 1.7 と共に Mobile Link サーバ,を実行するには、-jrepath オプションを
  libjvm.dylib ファイルのロケーション (フルパス) に設定します。たとえば、次のようになります。

  -sljava\(-jrepath `/usr/libexec/java_home -v 1.7`/jre/lib/server/libjvm.dylib\)


Relay Server
------------

o Using the Relay Server with Apache 2.4 is not recommended as it
  triggers the behavior identified in Apache Bugzilla issue 53555 (see
  https://bz.apache.org/bugzilla/show_bug.cgi?id=53555).
  Apache 2.2 is the recommended version.


Ultra Light
---------

o 現時点では何もありません。


オペレーティングシステムのサポート
------------------------

o RedHat Enterprise Linux 6 の Direct I/O および THP のサポート - Red Hat Linux 6 では、
  このオペレーティングシステムバージョンで導入された Transparent Huge Page (THP) 機能に
  バグがあり、Direct I/O と使用した場合に発現する可能性があります。SQL Anywhere で
  このバグを表すものとして最も可能性が高いのは、アサーション 200505 (X ページの障害の
  チェックサム) です。Red Hat bug 891857 は、この問題を追跡するために作成されました。

  To work around this issue, SQL Anywhere avoids using Direct I/O on this
  operating system. To use Direct I/O, disable THP by running following command:
       echo never > /sys/kernel/mm/redhat_transparent_hugepage/enabled

o 64-bit Linux support - Some 64-bit Linux operating systems do not include
  preinstalled 32-bit compatability libraries. To use 32-bit software,
  you may need to install 32-bit compatability libraries for your Linux
  distribution. For example, on Ubuntu, run the following command:
	sudo apt-get install ia32-libs

RedHat で次のコマンドを実行します。
	yum install glibc.i686
	yum install libXrenderer.so.1
	yum install libpk-gtk-module.so
	yum install libcanberra-gtk2.i686
	yum install gtk2-engines.i686

o dbsvc に対する Linux のサポート - dbsvc ユーティリティを使用するには、LSB init ファンクションが必要です。
一部の Linux オペレーティングシステムには、これらのファンクションがデフォルトでプレインストールされていません。
dbsvc を使用するには、Linux ディストリビューション用にこれらをインストールする必要があります。
たとえば、Fedora では次のコマンドを実行します。
	yum install redhat-lsb redhat-lsb.i686

o SELinux support - If you are having problems running SQL Anywhere on SELinux,
  then you have the following options:

o 共有ライブラリをロードできるようにラベルを変更します。この方法は、
    Red Hat Enterprise Linux 5 で機能しますが、SELinux の機能を使用
    できないという欠点があります。
	find $SQLANY17 -name "*.so" | xargs chcon -t textrel_shlib_t 2>/dev/null

o SQL Anywhere 17 に付属するポリシーをインストールします。インストールの
    selinux ディレクトリにポリシーのソースがあります。ポリシーの構築と
    インストールの指示については、そのディレクトリ内の
    README ファイルを参照してください。

  o Write your own policy. You can use the policy provided with
    SQL Anywhere 17 as a starting point.

  o 次のように入力して SELinux を無効にします。
/usr/sbin/setenforce 0

o スレッドとセマフォ - ソフトウェアで使用されているスレッドと
  セマフォの種類は重要です。システムによっては、これらの
  リソースが不足する可能性があります。

    o Linux、AIX、HP-UX、Mac OS X では、SQL Anywhere で
      pthreads (POSIX スレッド) と System V のセマフォが使用されます。

      Note: On platforms where System V semaphores are used, if the database
      server or a client application is terminated with SIGKILL, then System V
      semaphores are leaked. Clean them up by using the ipcrm command.
      In addition, client applications that terminate using the _exit() system
      call also leak System V semaphores unless the SQL Anywhere client
      libraries (such as ODBC and DBLib) are unloaded before this call.

o アラーム処理 - この機能は、非スレッド化アプリケーションの
  開発に SIGALRM または SIGIO ハンドラを使用している場合にのみ関係します。

  SQL Anywhere uses a SIGALRM and a SIGIO handler in non-threaded
  clients and starts up a repeating alarm (every 200 ms). For correct behavior,
  SQL Anywhere must be allowed to handle these signals.

  If you define a SIGALRM or SIGIO handler before loading any SQL Anywhere
  libraries, then SQL Anywhere chains to these handlers.
  If you define a handler after loading any SQL Anywhere libraries,
  then chain from the SQL Anywhere handlers.

  If you use the TCP/IP communication protocol, SQL Anywhere uses
  SIGIO handlers in only non-threaded clients. This handler is always
  installed, but it is used only if your application makes use of TCP/IP.

o Red Hat Enterprise Linux では、一部の私用文字が SQL Central、Interactive 
  SQL (dbisql)、Mobile Link プロファイラ、または
   SQL Anywhere モニタで表示されない場合があります。

  Red Hat Linux ディストリビューションに付属するどの TrueType
  フォントにも、ユニコードのコードポイント "U+E844" と "U+E863"
  (私用文字) のグリフはありません。問題の文字は簡体字中国語の
  文字で、Red Flag (中国語版 Linux) ディストリビューションで
  zysong.ttf (DongWen-Song) フォントに含まれます。

