SQL Anywhere 17.0 发行说明(用于 Unix 和 Mac OS X)

(c) 2015 SAP SE 或其关联公司版权所有。保留所有权利。


安装 SQL Anywhere 17
--------------------

1. 切换至已创建的目录，并通过运行
以下命令启动安装程序脚本:
        cd ga1700
        ./setup

   有关可用安装选项的完整列表，
请运行以下命令:
./setup -h

2. 按照安装程序中的说明操作。


安装说明
------------------

o 现在没有内容。


文档
-------------

文档可在 DocCommentXchange 上获取，网址为:
    http://dcx.sap.com

DocCommentXchange 是在 Web 上访问和讨论
SQL Anywhere 文档的在线社区。DocCommentXchange 是
SQL Anywhere 17 的缺省文档格式。


o The CREATE ODATA PRODUCER statement incorrectly states that the
  SERVICE ROOT clause is optional. This clause is required.


o The documentation about updating the DBD::SQLAnywhere Perl Driver
  is incorrect. For information about updating the driver, refer to
  %SQLANY17%\SDK\Perl\readme.txt.


o  UltraLite New Feature - A new parameter named 'kdf_iterations' is now available when you are
   using database encryption. The value is the number of iterations (in thousands)
   of the "key derivation function". This value is the database encryption
   passphrase (the DBKEY parameter) that is processed on database startup. The key
   derivation function makes it more difficult to access an encrypted database by
   prolonging each attack attempt. The default iteration count values are
   30 (thousand) on Mac OS X and 5 (thousand) elsewhere.

   There are two cases when an explicit setting is required:

   1. You are using a very slow device and UltraLite takes too
      long to start with encryption. For example: kdf_iterations=1

   2. You are using a high-end computer and want added security. For
      example, when running on a Windows or Linux desktop: kdf_iterations=100

   This parameter is specified only at database creation.


SQL Anywhere 论坛
-----------------

SQL Anywhere 论坛是一个 Web 站点，您可以
在其中提出及回答关于 SQL Anywhere 软件的问题，
并对他人的问题及回答进行评论和投票。可以通过以下网址访问 SQL Anywhere 论坛:
    http://sqlanywhere-forum.sap.com。


设置 SQL Anywhere 17 的环境变量
-------------------------------

每个使用该软件的用户都必须设置必要的 SQL Anywhere
环境变量。这些变量的设置取决于您使用的特定操作系统，
具体内容在文档中的“SQL Anywhere 服务器 - 数据库管理 >
数据库配置 > SQL Anywhere 环境变量”中讨论。


SQL Anywhere 17 的发行说明
--------------------------


SQL Anywhere 服务器
-------------------

o 现在没有内容。


管理工具
--------

o When installing SQL Anywhere on 64-bit Linux machines, the default option
  is to install the 64-bit version of the graphical administration tools
  (SQL Central, Interactive SQL, and the MobiLink Profiler).

  也可以选择安装 32 位管理工具。
  此选项仅适用于需要 32 位文件进行重新分发的 OEM 厂商。

  Running the 32-bit administration tools on 64-bit Linux is not supported.

o 要针对管理工具启用 Java Access Bridge，
  请编辑辅助功能属性文件并取消最后两行的注释。

  文件显示形式如下:
#
  # Load the Java Access Bridge class into the JVM
  #
  #assistive_technologies=com.sun.java.accessibility.AccessBridge
  #screen_magnifier_present=true

o To use the adminsitration tools on Mac OS X distributions, install
  Java 1.7. It is available from:

    http://www.oracle.com/technetwork/java/index.html

o The graphical administration tools Interactive SQL, SQL Central, and
  the MobiLink Profiler) can crash on startup if they are run on a Solaris
  SPARC 10 computer. This behavior occurs if the user is logged in with a
  Simplified Chinese, UTF-8 language setting (zh_CN.UTF-8) and is related
  o the input method editor (IME) that is used for that language.

  There are two workarounds:

  选项 1:
使用其它语言设置登录，如 "zh_CN.GB18030"。

  Option 2:
    If you have to use zh_CN.UTF-8, then terminate the IME processes before
    running the graphical administration tools by running the following
    command from a Terminal window:
        pkill iiim*


o If characters do not display correctly in the Interactive SQL utility when
  you input data from a UTF8BIN database on SuSE Linux systems, you need to
  install a Unicode font.

   1. Go to "All Settings".
   2. In the "System" category, click "YaST" (its icon
      is an aardvark). Provide the root password to start YaST.
   3. In YaST, click "Software Management" (its icon is a box that is half
      white and half green with a Novell red letter "N" on it).
   4. In the "YaST 2" window, type "unicode font", and then
      click "Search".
   5. In the "Package" list (top right corner of the window),
      check everything ("efont-unicode-bitmap-fonts", "arphic-ukai-fonts",
      and so on), and then click the "Accept" button in the bottom right
      corner of the window.
   6. Reboot and retry the operation.


MobiLink
--------

o The MobiLink server requires an ODBC driver to communicate with the
  consolidated databases. The recommended ODBC drivers for a supported
  consolidated database can be found at:
    http://scn.sap.com/docs/DOC-63337

o 有关 MobiLink 支持的平台的信息，请参见:
    http://scn.sap.com/docs/DOC-35654

o 要通过 Mac OS X 中的 Java 1.7 运行 MobiLink 服务器，
  将 -jrepath 选项设置为 libjvm.dylib 文件的完整路径位置。例如:

  -sljava\(-jrepath `/usr/libexec/java_home -v 1.7`/jre/lib/server/libjvm.dylib\)


Relay Server
------------

o Using the Relay Server with Apache 2.4 is not recommended as it
  triggers the behavior identified in Apache Bugzilla issue 53555 (see
  https://bz.apache.org/bugzilla/show_bug.cgi?id=53555).
  Apache 2.2 is the recommended version.


UltraLite
---------

o 现在没有内容。


操作系统支持
------------------------

o RedHat Enterprise Linux 6 Direct I/O 和 THP 支持 - 当和 Direct I/O 一起
  使用时，Red Hat Linux 6 在该操作系统版本所引入的大内存页 (THP) 功能中
  可能存在错误。该错误在 SQL Anywhere 中最有可能的表现形式为
  声明 200505(页面 X 上的校验和失败)。
已创建 Red Hat 错误 891857 跟踪该问题。

  To work around this issue, SQL Anywhere avoids using Direct I/O on this
  operating system. To use Direct I/O, disable THP by running following command:
       echo never > /sys/kernel/mm/redhat_transparent_hugepage/enabled

o 64-bit Linux support - Some 64-bit Linux operating systems do not include
  preinstalled 32-bit compatability libraries. To use 32-bit software,
  you may need to install 32-bit compatability libraries for your Linux
  distribution. For example, on Ubuntu, run the following command:
	sudo apt-get install ia32-libs

  在 RedHat 上运行:
	yum install glibc.i686
	yum install libXrenderer.so.1
	yum install libpk-gtk-module.so
	yum install libcanberra-gtk2.i686
	yum install gtk2-engines.i686

o Linux 对 dbsvc 的支持 - dbsvc 实用程序需要使用 LSB 初始化函数。
某些 Linux 操作系统在缺省情况下不预安装这些函数。
要使用 dbsvc，需要为 Linux 发布版本安装这些函数。
例如，在 Fedora 上运行以下命令:
	yum install redhat-lsb redhat-lsb.i686

o SELinux support - If you are having problems running SQL Anywhere on SELinux,
  then you have the following options:

o 重新标记共享库，以便可以加载。该解决方案
    在 Red Hat Enterprise Linux 5 上有效，但缺点是不使用
    SELinux 功能。
	find $SQLANY17 -name "*.so" | xargs chcon -t textrel_shlib_t 2>/dev/null

o 安装随 SQL Anywhere 17 提供的策略。在安装的 selinux 目录
中有策略源。请参见此目录中
的 README 文件来了解构建和安装此策略的说明。

  o Write your own policy. You can use the policy provided with
    SQL Anywhere 17 as a starting point.

  o 禁用 SELinux:
/usr/sbin/setenforce 0

o 线程和信号 – 软件中使用的线程和信号的类型
非常重要，因为某些系统可能耗尽这些资源。


    o 在 Linux、AIX、HP-UX 和 Mac OS X 中，SQL Anywhere 使用
pthreads(POSIX 线程)和系统 V 信号。

      Note: On platforms where System V semaphores are used, if the database
      server or a client application is terminated with SIGKILL, then System V
      semaphores are leaked. Clean them up by using the ipcrm command.
      In addition, client applications that terminate using the _exit() system
      call also leak System V semaphores unless the SQL Anywhere client
      libraries (such as ODBC and DBLib) are unloaded before this call.

o 警报处理 – 仅当开发非线程应用程序
  并使用 SIGALRM 或 SIGIO 处理程序时，该功能才有用。

  SQL Anywhere uses a SIGALRM and a SIGIO handler in non-threaded
  clients and starts up a repeating alarm (every 200 ms). For correct behavior,
  SQL Anywhere must be allowed to handle these signals.

  If you define a SIGALRM or SIGIO handler before loading any SQL Anywhere
  libraries, then SQL Anywhere chains to these handlers.
  If you define a handler after loading any SQL Anywhere libraries,
  then chain from the SQL Anywhere handlers.

  If you use the TCP/IP communication protocol, SQL Anywhere uses
  SIGIO handlers in only non-threaded clients. This handler is always
  installed, but it is used only if your application makes use of TCP/IP.

o 在 Red Hat Enterprise Linux 上，某些专用字符
  在 SQL Central、Interactive SQL (dbisql)、MobiLink 分析器或 SQL
  Anywhere 监控器中可能不显示。

  对于 Unicode 代码点 "U+E844" 和 "U+E863"(指定为专用字符)，
在随 Red Hat Linux 发布版本提供的任何 truetype 字体中，
均不提供轮廓。上述字符是简体中文字符，在 Red Flag (中文版 Linux)发布版本
中作为 zysong.ttf (DongWen-Song) 字体的一部分提供。


