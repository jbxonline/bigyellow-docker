SQL Anywhere 17.0 - Notes de mise à jour pour Unix, Linux et Mac OS X

Copyright (c) 2015 SAP SE ou société affiliée SAP. Tous droits réservés.


Installation de SQL Anywhere 17
-------------------------------

1. Dans le répertoire créé, lancez le script d'installation en exécutant
   les commandes suivantes :
        cd ga1700
        ./setup

   Pour obtenir la liste complète des options d'installation disponibles,
   exécutez cette commande :
        ./setup -h

2. Suivez les instructions du programme d'installation.


Notes d'installation
--------------------

o Aucune information disponible.


Documentation
-------------

La documentation est disponible sur DocCommentXchange :
    http://dcx.sap.com

DocCommentXchange est un site communautaire sur lequel vous pouvez consulter
et commenter la documentation de SQL Anywhere. DocCommentXchange est le format
de documentation par défaut pour SQL Anywhere 17.


o The CREATE ODATA PRODUCER statement incorrectly states that the
  SERVICE ROOT clause is optional. This clause is required.


o The documentation about updating the DBD::SQLAnywhere Perl Driver
  is incorrect. For information about updating the driver, refer to
  %SQLANY17%\SDK\Perl\readme.txt.


o  UltraLite New Feature - A new parameter named 'kdf_iterations' is now available when you are
   using database encryption. The value is the number of iterations (in thousands)
   of the "key derivation function". This value is the database encryption
   passphrase (the DBKEY parameter) that is processed on database startup. The key
   derivation function makes it more difficult to access an encrypted database by
   prolonging each attack attempt. The default iteration count values are
   30 (thousand) on Mac OS X and 5 (thousand) elsewhere.

   There are two cases when an explicit setting is required:

   1. You are using a very slow device and UltraLite takes too
      long to start with encryption. For example: kdf_iterations=1

   2. You are using a high-end computer and want added security. For
      example, when running on a Windows or Linux desktop: kdf_iterations=100

   This parameter is specified only at database creation.


Forum SQL Anywhere
------------------

Le forum SQL Anywhere est un site Web sur lequel vous pouvez poser
des questions sur le logiciel SQL Anywhere et apporter des réponses,
ainsi que commenter les questions et réponses des autres participants.
Rendez-vous sur le forum SQL Anywhere à l'adresse :
    http://sqlanywhere-forum.sap.com.


Configuration des variables d'environnement pour SQL Anywhere 17
----------------------------------------------------------------

Les variables d'environnement de SQL Anywhere doivent être définies
préalablement à l'utilisation du logiciel. Leur paramétrage dépend du système
d'exploitation. Pour le connaître, consultez la section "Database
Configuration" > "SQL Anywhere environment variables" du manuel "SQL Anywhere
Server - Database Administration".


SQL Anywhere 17 - Notes de mise à jour
--------------------------------------


Serveur SQL Anywhere
--------------------

o Aucune information disponible.


Outils d'administration
-----------------------

o When installing SQL Anywhere on 64-bit Linux machines, the default option
  is to install the 64-bit version of the graphical administration tools
  (SQL Central, Interactive SQL, and the MobiLink Profiler).

  Vous avez également la possibilité de les installer en version 32 bits,
   cette option étant réservée aux OEM dont la redistribution inclut des
  fichiers 32 bits.

  Running the 32-bit administration tools on 64-bit Linux is not supported.

o Pour activer Java Access Bridge pour les outils d'administration,
  modifiez le fichier accessibility.properties en supprimant la mise en
  commentaire des deux dernières lignes.

  Le fichier se présente ainsi :
  #
  # Load the Java Access Bridge class into the JVM
  #
  #assistive_technologies=com.sun.java.accessibility.AccessBridge
  #screen_magnifier_present=true

o To use the adminsitration tools on Mac OS X distributions, install
  Java 1.7. It is available from:

    http://www.oracle.com/technetwork/java/index.html

o The graphical administration tools Interactive SQL, SQL Central, and
  the MobiLink Profiler) can crash on startup if they are run on a Solaris
  SPARC 10 computer. This behavior occurs if the user is logged in with a
  Simplified Chinese, UTF-8 language setting (zh_CN.UTF-8) and is related
  o the input method editor (IME) that is used for that language.

  There are two workarounds:

  Option 1 :
    Se connecter en utilisant un autre paramètre de langue, comme "zh_CN.GB18030".

  Option 2:
    If you have to use zh_CN.UTF-8, then terminate the IME processes before
    running the graphical administration tools by running the following
    command from a Terminal window:
        pkill iiim*


o If characters do not display correctly in the Interactive SQL utility when
  you input data from a UTF8BIN database on SuSE Linux systems, you need to
  install a Unicode font.

   1. Go to "All Settings".
   2. In the "System" category, click "YaST" (its icon
      is an aardvark). Provide the root password to start YaST.
   3. In YaST, click "Software Management" (its icon is a box that is half
      white and half green with a Novell red letter "N" on it).
   4. In the "YaST 2" window, type "unicode font", and then
      click "Search".
   5. In the "Package" list (top right corner of the window),
      check everything ("efont-unicode-bitmap-fonts", "arphic-ukai-fonts",
      and so on), and then click the "Accept" button in the bottom right
      corner of the window.
   6. Reboot and retry the operation.


MobiLink
--------

o The MobiLink server requires an ODBC driver to communicate with the
  consolidated databases. The recommended ODBC drivers for a supported
  consolidated database can be found at:
    http://scn.sap.com/docs/DOC-63337

o Pour connaître les plates-formes prises en charge par MobiLink, consultez :
    http://scn.sap.com/docs/DOC-35654

o Pour exécuter le serveur MobiLink avec Java 1.7 sur Mac OS X, définissez
  le chemin complet du fichier libjvm.dylib comme valeur de l'option -jrepath. Par exemple :

  -sljava\(-jrepath `/usr/libexec/java_home -v 1.7`/jre/lib/server/libjvm.dylib\)


Relay Server
------------

o Using the Relay Server with Apache 2.4 is not recommended as it
  triggers the behavior identified in Apache Bugzilla issue 53555 (see
  https://bz.apache.org/bugzilla/show_bug.cgi?id=53555).
  Apache 2.2 is the recommended version.


UltraLite
---------

o Aucune information disponible.


Système d'exploitation requis
-----------------------------

o Prise en charge des THP et des E/S directes de RedHat EnterpriseLinux 6 - Il
  est possible qu'un bug se produise avec la nouvelle fonctionnalité THP
  (transparent huge pages, pages très volumineuses transparentes) de cette
  version de système d'exploitation lorsqu'elle est utilisée avec des E/S
  directes. Ce bug se traduira probablement par une assertion 200505 dans SQL
  Anywhere (erreur de checksum à la page X). Pour le suivi du problème, le bug
  Red Hat 891857 a été créé.

  To work around this issue, SQL Anywhere avoids using Direct I/O on this
  operating system. To use Direct I/O, disable THP by running following command:
       echo never > /sys/kernel/mm/redhat_transparent_hugepage/enabled

o 64-bit Linux support - Some 64-bit Linux operating systems do not include
  preinstalled 32-bit compatability libraries. To use 32-bit software,
  you may need to install 32-bit compatability libraries for your Linux
  distribution. For example, on Ubuntu, run the following command:
	sudo apt-get install ia32-libs

  Sur RedHat, exécutez :
	yum install glibc.i686
	yum install libXrenderer.so.1
	yum install libpk-gtk-module.so
	yum install libcanberra-gtk2.i686
	yum install gtk2-engines.i686

o Prise en charge de dbsvc sur Linux - Les fonctions LSB init-functions sont
  nécessaires pour cet utilitaire. Or, certains systèmes d'exploitation ne
  les pré-installent pas par défaut. Pour utiliser dbsvc, vous devez donc
  installer les fonctions adaptées à votre distribution Linux. Par exemple,
  sur Fedora, exécutez cette commande :
	yum install redhat-lsb redhat-lsb.i686

o SELinux support - If you are having problems running SQL Anywhere on SELinux,
  then you have the following options:

o Modifiez l'étiquette des bibliothèques partagées pour en permettre le
    chargement. Si cette solution fonctionne sur Red Hat Enterprise Linux 5,
    elle a pour inconvénient de ne pas utiliser les fonctionnalités SELinux.
    find $SQLANY17 -name "*.so" | xargs chcon -t textrel_shlib_t 2>/dev/null

o Installez la stratégie fournie avec SQL Anywhere 17. Vous trouverez des
    sources de stratégie dans le répertoire selinux de votre installation.
    Le fichier README présent dans ce répertoire fournit des instructions de
    construction et d'installation de stratégie.

  o Write your own policy. You can use the policy provided with
    SQL Anywhere 17 as a starting point.

  o Désactivez SELinux :
        /usr/sbin/setenforce 0

o Threads et sémaphores - Les types de thread et de sémaphore utilisés dans
  le logiciel sont assez déterminants dans la mesure où ces ressources peuvent
  s'épuiser sur certains systèmes.

    o Sur Linux, AIX, HP-UX et Mac OS X, SQL Anywhere utilise des threads
      pthreads (threads POSIX) et des sémaphores System V.

      Note: On platforms where System V semaphores are used, if the database
      server or a client application is terminated with SIGKILL, then System V
      semaphores are leaked. Clean them up by using the ipcrm command.
      In addition, client applications that terminate using the _exit() system
      call also leak System V semaphores unless the SQL Anywhere client
      libraries (such as ODBC and DBLib) are unloaded before this call.

o Gestion des alarmes - Cette fonctionnalité vous concerne uniquement si vous
  développez des applications sans thread et utilisez les gestionnaires
  SIGALRM ou SIGIO.

  SQL Anywhere uses a SIGALRM and a SIGIO handler in non-threaded
  clients and starts up a repeating alarm (every 200 ms). For correct behavior,
  SQL Anywhere must be allowed to handle these signals.

  If you define a SIGALRM or SIGIO handler before loading any SQL Anywhere
  libraries, then SQL Anywhere chains to these handlers.
  If you define a handler after loading any SQL Anywhere libraries,
  then chain from the SQL Anywhere handlers.

  If you use the TCP/IP communication protocol, SQL Anywhere uses
  SIGIO handlers in only non-threaded clients. This handler is always
  installed, but it is used only if your application makes use of TCP/IP.

o Sur Red Hat Enterprise Linux, certains caractères d'usage privé ne
  s'affichent pas dans SQL Central, Interactive SQL (dbisql), le Profileur
  MobiLink ou le Moniteur SQL Anywhere.

  Concernant les points de code Unicode "U+E844" et "U+E863" (désignés comme
  des caractères d'usage privé), aucun glyphe n'est fourni dans aucune des
  polices TrueType fournies avec la distribution Red Hat Linux. Il s'agit de
  caractères chinois simplifiés disponibles dans la distribution Red Flag
  (Linux en chinois) et qui font partie de la police zysong.ttf (DongWen-Song).

