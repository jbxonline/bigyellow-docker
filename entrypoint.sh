#!/bin/bash
cd /var/www/vhosts/bigyellow

if [ ! -d "cache" ]; then
	mkdir -p cache/zend
fi

if [ ! -d "logs" ]; then
	mkdir logs
fi

if [ ! -f "config/config.ini" ]; then
	cp config/config_template.ini config/config.ini
fi

chown -R root.apache ./
chmod -R ug+rwX cache logs

cd assets && npm install && bower install --allow-root && grunt build

cd /var/www/vhosts/bigyellow && npm install && npm run build-prod

/usr/sbin/httpd -D FOREGROUND